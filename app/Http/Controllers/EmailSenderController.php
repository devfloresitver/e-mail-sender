<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use Storage;
use App\EmailWithAttachments;
use Mail;
use App\Email;

class EmailSenderController extends Controller
{

	protected $from;
	protected $to;
	protected $msg;
	protected $cc;
	protected $bcc;
	protected $subject;

    public function index(){
    	return view('index');
    }

    public function createNewMail(Request $aRequest){
    	$this->from = $aRequest->from;
    	$this->subject = $aRequest->subject;
    	$this->to = $aRequest->to;
    	$this->cc = $aRequest->cc;
    	$this->msg = nl2br($aRequest->msg);
    	$response_status = 0;

    	//dd($this->msg);

    	$registeredEmail = $this->registerEmail();
    	$savedFiles = null;

    	if($aRequest->has('attachments')){
    		$attachments = $aRequest->file('attachments');
    		$savedFiles = $this->attachFiles($attachments);
    	}

    	$emailSent = $this->sendEmail($savedFiles);

    	if($emailSent && $registeredEmail){
    		$response_status = 1;
    	}

    	return back()->with('status', $response_status);    	
    }

    protected function registerEmail(){
    	$newEmail = new Email();
    	$newEmail->from = $this->from;
    	$newEmail->subject = $this->subject;
    	$newEmail->to = $this->to;
    	$newEmail->cc = $this->cc;
    	$newEmail->msg = $this->msg;
    	$newEmail->save();
    	return true;
    }

    protected function attachFiles($attachments){
    	$filePaths = [];
    	foreach ($attachments as $file) {
    		$fileName = $file->getClientOriginalName();
    		$file->move(storage_path('filess'), $fileName);
    		$filepath = storage_path('filess').'\\'.$fileName;
    		array_push($filePaths, $filepath);
    	}
    	
    	return $filePaths;

    }

    private function sendEmail($filePaths){
    	$emailData = $this->buildEmailInfo();
    	$from = $this->from;
        $subject = $this->subject;
        $view = "email";
        $sCC = $this->cc;
        $bAttachments = false;

        if($filePaths){
        	$bAttachments = true;
        }

        Mail::to($this->to)->send(new EmailWithAttachments($emailData, $this->subject, $view, $filePaths, $sCC, $bAttachments));

        return true;
    }

    protected function buildEmailInfo(){
    	$emailData = [
    			'msg' => $this->msg,
    		];

    	return $emailData;
    }
}
