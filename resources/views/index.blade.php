<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Email Sender</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
	<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>
</head>
<body>

	<div class="jumbotron">
	  <h1 class="display-4">Send e-mail</h1>
	  	<form id="frmEmail" method="post" enctype="multipart/form-data" action="{{route('mail.send')}}">
	  	  @csrf()
	  	  <div class="form-row">
	  	  	<div class="form-group col-md-12">
		      <label for="from">Subject</label>
		      <input type="text" class="form-control" name="subject" id="subject" placeholder="subject" required>
		    </div>
	  	  </div>
		  <div class="form-row">
		    <div class="form-group col-md-6">
		      <label for="from">From</label>
		      <input type="email" class="form-control" name="from" id="from" placeholder="from" required>
		    </div>
		    <div class="form-group col-md-6">
		      <label for="to">To</label>
		      <input type="email" class="form-control" name="to" id="to" placeholder="to" required>
		    </div>
		  </div>
		  <div class="form-row">
		    <div class="form-group col-md-6">
	    	 <label for="cc">CC</label>
		    <input type="text" class="form-control" name="cc" id="cc" placeholder="cc" required>
		    </div>
		    <div class="form-group col-md-6">
	    	 <label for="file">Attach Files</label>
		    <input type="file" name="attachments[]" id="file" class="form-control-file"multiple >
		    </div>
		</div>
		{{--  <div class="form-group">
		    <label for="inputAddress2">CCO</label>
		    <input type="hidden" class="form-control" name="cco" placeholder="cco">
		  </div>
		  --}}
		  <div class="form-row">
		    <div class="form-group col-md-12">
		      <label for="msg">Message</label>
		      <textarea name="msg" id="msg" placeholder="message" class="form-control" required></textarea>
		    </div>
		  </div>
  	  	 <button type="submit" class="btn btn-primary float-right mb-3"  id="send">Send</button>
	   </form>
	   
	   <div class="container">
	   	<hr>
	   		@if(session('status') === 1)
	   		<div class="alert alert-success mt-3" role="alert">
		  		E-mail sent successfully.
			</div>
	   		@elseif(session('status') === 0)
	   		<div class="alert alert-danger mt-3" role="alert">
			  	Error while sending e-mail.
			</div>
			@else

	   		@endif
	   </div>
	</div>

	<script>
		$(document).ready(function() {
           $('#msg').summernote({
				toolbar: [
				  ['style', ['style']],
				  ['font', ['bold', 'underline', 'clear']],
				  ['fontname', ['fontname']],
				  ['color', ['color']],
				  ['para', ['ul', 'ol', 'paragraph']],
				  ['table', ['table']],
				  ['insert', ['link', 'picture', 'video']],
				  ['view', ['fullscreen', 'codeview', 'help']],
				],
				popover: {
				   air: []
				}         
			});
        });
	</script>
	
</body>
</html>